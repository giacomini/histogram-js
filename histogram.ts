// function typeOf (obj) {
//   return {}.toString.call(obj).split(' ')[1].slice(0, -1).toLowerCase();
// }

/// <reference path="google.visualization.d.ts"/>

type Bin = number[];

interface Pixel {
  x: number
  y: number
  c: Array<Bin>
}

function bins_to_array(bins: Bin[]): number[] {
  let result = [0];
  for (let i = 0; i != bins.length; ++i) {
    let [x, y] = bins[i];
    result[x] = y;
  }
  result[16383] = 0;
  return result;
}

function array_to_bins(a: number[]): Array<Bin> {
  let result = a.map((e, i) => {
    let b: Bin = [i, e];
    return b;
  });
  return result;
}

// this sum is wrong, because it doesn't consider undefined entries in a1 that are however defined in a2
function sum(a1: number[], a2: number[]) {
  return a1.map((e, i) => {
    return e + (a2[i] ? a2[i] : 0);
  });
}

function readData(response: string) {
  let result = JSON.parse(response);
  return result;
}

function show_chart(data) {

  let options = {
    title: 'all counts',
    width: 1000,
    height: 300,
  };

  console.time('show chart');
  let chart = new google.visualization.ColumnChart(document.getElementById('chart'));
  chart.draw(data, options);
  console.timeEnd('show chart');
}

function drawData(pixels) {
  let data = new google.visualization.DataTable();
  data.addColumn('number', 'Channel');
  data.addColumn('number', 'Count');

  console.time('sum counts across all pixels');
  let counts = pixels[0].c;
  let arr = bins_to_array(counts);
  for (let i = 1; i < pixels.length; ++i) {
    arr = sum(arr, bins_to_array(pixels[i].c));
  }
  let bins = array_to_bins(arr).filter((e) => { return !!e; });
  console.timeEnd('sum counts across all pixels');
  console.time('add row to chart');
  let data_row = bins.map((b) => {
    return b;
  })
  data.addRows(data_row);
  console.timeEnd('add row to chart');

  let options = {
    title: 'all counts',
    width: 1500,
    height: 500,
    vAxis: {
      //      logScale: true
    }
  };

  console.time('draw chart');
  let chart = new google.visualization.ColumnChart(document.getElementById('chart'));
  chart.draw(data, options);
  console.timeEnd('draw chart');
}

function drawData3(pixels) {
  console.log('in drawData3');
  console.time('merge counts across all pixels')
  let dt = new google.visualization.DataTable(
    {
      cols: [{ type: 'number', label: 'Channel' }, { type: 'number', label: 'Count' }]
    });
  for (let i = 0; i < pixels.length; ++i) {
    dt.addRows(pixels[i].c);
    dt = google.visualization.DataTable
      = google.visualization.data.group(
        dt, [0], [{ 'column': 1, 'aggregation': google.visualization.data.sum, 'type': 'number' }]
      );
  }
  console.timeEnd('merge counts across all pixels')

  show_chart(dt);
}

function drawData2(pixels) {
  console.log('in drawData2');
  // join all arrays
  console.time('merge counts across all pixels');
  let counts = [[{ type: 'number', label: 'Channel' }, { type: 'number', label: 'Count' }]];
  for (let i = 0; i < 1000; ++i) {
 //   console.log(i, counts.length);
    counts = counts.concat(pixels[i].c);
  }
  console.log(counts.length);
  console.timeEnd('merge counts across all pixels');

  console.time('create data table');
  let dt = google.visualization.arrayToDataTable(counts);
  let data: google.visualization.DataTable
    = google.visualization.data.group(
      dt, [0], [{ 'column': 1, 'aggregation': google.visualization.data.sum, 'type': 'number' }]
    );
  
  console.timeEnd('create data table');

  let options = {
    title: 'all counts',
    width: 1000,
    height: 300,
  };

  console.time('draw chart');
  let chart = new google.visualization.ColumnChart(document.getElementById('chart'));
  chart.draw(data, options);
  console.timeEnd('draw chart');
}

function drawChart(): void {

  let xmlhttp = new XMLHttpRequest();

  xmlhttp.onload = function () {
    if (!(this.readyState == 4 && this.status == 200)) {
      console.log("page is not fully loaded: " + this.readyState + ' ' + this.status);
      return;
    }
    let pixels: Pixel[] = readData(this.responseText);
    drawData3(pixels);
  }

  let url = "out.json";
  xmlhttp.open("GET", url, true);
  xmlhttp.send();
}

function draw_test() : void {
  console.time('create DataTable');
  // arrayToDataTable: 2 ms
  // let data = google.visualization.arrayToDataTable([
  //   [{ type: 'number', label: 'Channel' }, { type: 'number', label: 'Count' }],
  //   [1,2],[3,4],[4,2]
  //   ]);
  // DataTable constructed with literal: 1 ms
  // DataTable followed by addColumn/Rows: 6 ms
  let data = new google.visualization.DataTable(
    {
      cols: [{ type: 'number', label: 'Channel' }, { type: 'number', label: 'Count' }],
      rows: [
        { c: [{ v: 1 }, { v: 2 }] },
        { c: [{ v: 3 }, { v: 4 }] },
        { c: [{ v: 4 }, { v: 2 }] }
      ]
    }
  );
  // data.addColumn('number', 'Channel');
  // data.addColumn('number', 'Count');
  // data.addRows([[1,2],[3,4],[4,2]]);
  console.timeEnd('create DataTable');

  let options = {
    title: 'all counts',
    width: 1000,
    height: 300,
  };

  let chart = new google.visualization.ColumnChart(document.getElementById('chart'));
  chart.draw(data, options);
}

function merge_test() : void {
  console.time('create DataTable');
  let d1 = new google.visualization.DataTable(
    {
      cols: [{ type: 'number', label: 'Channel' }, { type: 'number', label: 'Count' }],
      rows: [
        { c: [{ v: 1 }, { v: 2 }] },
        { c: [{ v: 3 }, { v: 4 }] }
      ]
    }
  );
  console.timeEnd('create DataTable');
  let j1 = JSON.parse(d1.toJSON());
  let d2 = new google.visualization.DataTable(
    {
      cols: [{ type: 'number', label: 'Channel' }, { type: 'number', label: 'Count' }],
      rows: [
        { c: [{ v: 2 }, { v: 2 }] },
        { c: [{ v: 3 }, { v: 2 }] }
      ]
    }
  );
  let j2 = JSON.parse(d2.toJSON());
  j1.rows = j1.rows.concat(j2.rows);
  let d3 = new google.visualization.DataTable(j1);
  console.log(d3.toJSON());
  let d4: google.visualization.DataTable
    = google.visualization.data.group(
      d3, [0], [{ 'column': 1, 'aggregation': google.visualization.data.sum, 'type': 'number' }]
    );
  console.log(d4.toJSON());
  let options = {
    title: 'all counts',
    width: 1000,
    height: 300,
  };

  let chart = new google.visualization.ColumnChart(document.getElementById('chart'));
  chart.draw(d4, options);
}

google.charts.load('current', { 'packages': ['corechart'] });
google.charts.setOnLoadCallback(drawChart);
// google.charts.setOnLoadCallback(merge_test);
